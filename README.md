# Customer API Gateway

## Main info

Name: Customer API Gateway  
Domain: Customer GUI  
Purpose: Backend-for-Frontend (BFF) for the client-side application

## Development

Instructions describing how to deploy, launch and test the service on a local machine can be found in a separate document at [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

The regulations for working on tasks are also in [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Service structure

You can read about the service structure [here](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Dependencies

| Name                    | Description                                                   | Environment variables                                                                                                                                                                                    |
|-------------------------|---------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Ensi services**       | **Ensi services with which this service communicates**        |
| Catalog                 | Ensi Offers<br/>Ensi PIM<br/>Ensi Catalog Cache<br/>Ensi Feed | CATALOG_OFFERS_SERVICE_HOST<br/>CATALOG_PIM_SERVICE_HOST<br/>CATALOG_CATALOG_CACHE_SERVICE_HOST<br/>CATALOG_FEED_SERVICE_HOST                                                                            |
| Communication           | Ensi Communication Manager                                    | COMMUNICATION_COMMUNICATION_SERVICE_HOST                                                                                                                                                                 |
| Customers               | Ensi Customers<br/>Ensi Customer Auth<br/>Ensi CRM            | CUSTOMERS_CUSTOMERS_SERVICE_HOST<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_HOST<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_ID<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_SECRET<br/>CUSTOMERS_CRM_SERVICE_HOST |
| CMS                     | Ensi Cms                                                      | CMS_CMS_SERVICE_HOST                                                                                                                                                                                     |
| Logistic                | Ensi Logistic                                                 | LOGISTIC_LOGISTIC_SERVICE_HOST                                                                                                                                                                           |
| Orders                  | Ensi OMS<br/>Ensi Baskets                                     | ORDERS_OMS_SERVICE_HOST<br/>ORDERS_BASKETS_SERVICE_HOST                                                                                                                                                  |
| Units                   | Ensi Business Units                                           | UNITS_BU_SERVICE_HOST                                                                                                                                                                                    |
| Reviews                 | Ensi Reviews                                                  | REVIEWS_REVIEWS_SERVICE_HOST                                                                                                                                                                             |
| **Ensi Cloud services** | **Ensi Cloud services with which this service communicates**  |
| Cloud API               | Ensi Cloud API                                                | CLOUD_API_SERVICE_HOST                                                                                                                                                                                   |

## Environments

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/customer-gui/job/customer-api-gateway/  
URL: https://customer-api-gateway-master-dev.ensi.tech/docs/swagger

### Preprod

N/A

### Prod

N/A

## Contacts

The team supporting this service: https://gitlab.com/groups/greensight/ensi/-/group_members
Email: mail@greensight.ru

## License

[Open license for the right to use the Greensight Ecom Platform (GEP) computer program](LICENSE.md).
