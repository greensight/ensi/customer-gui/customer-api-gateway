<?php

namespace Tests;

use Ensi\BasketsClient\Api\CalculatorsBasketsApi;
use Ensi\BasketsClient\Api\CustomerBasketsApi as BasketApi;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi as CatalogCacheOffersApi;
use Ensi\CloudApiSdk\Api\CatalogApi as CloudCatalogApi;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\CustomerAuthClient\Configuration;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\FeedClient\Api\CloudIntegrationsApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Catalog Cache
    public function mockCatalogCacheOffersApi(): MockInterface|CatalogCacheOffersApi
    {
        return $this->mock(CatalogCacheOffersApi::class);
    }
    // endregion

    // region service Offers
    public function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }
    // endregion

    // region service CMS
    public function mockCmsPagesApi(): MockInterface|OffersApi
    {
        return $this->mock(PagesApi::class);
    }

    public function mockCmsSeoTemplatesApi(): MockInterface|SeoTemplatesApi
    {
        return $this->mock(SeoTemplatesApi::class);
    }
    // endregion

    // region cms banners
    public function mockCmsBannersApi(): MockInterface|BannersApi
    {
        return $this->mock(BannersApi::class);
    }
    // endregion

    // region service PIM
    public function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockPimCategoriesApi(): MockInterface|CategoriesApi
    {
        return $this->mock(CategoriesApi::class);
    }

    protected function mockPimBrandsApi(): MockInterface|BrandsApi
    {
        return $this->mock(BrandsApi::class);
    }
    // endregion

    // region service Feed
    protected function mockFeedCloudIntegrationsApi(): MockInterface|CloudIntegrationsApi
    {
        return $this->mock(CloudIntegrationsApi::class);
    }
    // endregion

    // =============== Cloud API SDK ===============
    // region service Cloud
    protected function mockCloudSdkApi(): MockInterface|CloudCatalogApi
    {
        return $this->mock(CloudCatalogApi::class);
    }
    // endregion

    // =============== Customers ===============

    // region service Customer Auth
    public function mockCustomerAuthOauthApi(): MockInterface|OauthApi
    {
        return $this->mock(OauthApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }

    public function mockCustomerAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }
    // endregion

    // region service Customers
    protected function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }
    // endregion

    // region service CRM
    protected function mockCustomersCustomerFavoritesApi(): MockInterface|CustomerFavoritesApi
    {
        return $this->mock(CustomerFavoritesApi::class);
    }
    // endregion

    // =============== Orders ===============

    // region Baskets
    public function mockBasketsBasketApi(): MockInterface|BasketApi
    {
        return $this->mock(BasketApi::class);
    }

    public function mockBasketsCalculatorsApi(): MockInterface|CalculatorsBasketsApi
    {
        return $this->mock(CalculatorsBasketsApi::class);
    }

    // region OMS
    public function mockOrdersOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }
    // endregion

    // region service bu
    public function mockBuSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }

    public function mockBuStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }
    // endregion

    // region service reviews-reviews
    public function mockReviewsReviewsApi(): MockInterface|ReviewsApi
    {
        return $this->mock(ReviewsApi::class);
    }
    // endregion

    // region service logistic-logistic
    public function mockDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }
    // endregion

    // region service Communication
    protected function mockCommunicationNotificationsApi(): MockInterface|NotificationsApi
    {
        return $this->mock(NotificationsApi::class);
    }
    // endregion
}
