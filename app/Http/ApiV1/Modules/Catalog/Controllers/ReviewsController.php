<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Reviews\CreateReviewAction;
use App\Domain\Catalog\Actions\Reviews\DeleteReviewAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Reviews\ReviewsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Reviews\CreateReviewRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Reviews\ReviewsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReviewsController
{
    public function create(CreateReviewRequest $request, CreateReviewAction $action): EmptyResource
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function delete(int $id, DeleteReviewAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(ReviewsQuery $query): AnonymousResourceCollection
    {
        return ReviewsResource::collectPage($query->get());
    }
}
