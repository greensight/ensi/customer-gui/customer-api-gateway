<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Domain\Catalog\Actions\Offers\SearchOffersAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ElasticOffersQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\SearchOffersRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Elastic\ElasticOffersResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OffersController
{
    public function get(int $id, ElasticOffersQuery $query): ElasticOffersResource
    {
        return new ElasticOffersResource($query->find($id));
    }

    public function search(SearchOffersRequest $request, SearchOffersAction $action): AnonymousResourceCollection
    {
        return ElasticOffersResource::collectPage($action->execute($request));
    }
}
