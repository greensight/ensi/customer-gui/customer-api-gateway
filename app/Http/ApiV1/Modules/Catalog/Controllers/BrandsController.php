<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Modules\Catalog\Queries\Brands\BrandsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Brands\BrandsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandsController
{
    public function get(int $id, BrandsQuery $query): BrandsResource
    {
        return BrandsResource::make($query->find($id));
    }

    public function search(BrandsQuery $query): AnonymousResourceCollection
    {
        return BrandsResource::collectPage($query->get());
    }
}
