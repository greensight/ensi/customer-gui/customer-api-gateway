<?php

use App\Domain\Catalog\Tests\Factories\Reviews\ReviewFactory;
use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\CreateReviewRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\ReviewsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/reviews-create success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockReviewsReviewsApi()->allows([
        'createReview' => ReviewFactory::new()->makeResponseOne(),
    ]);
    $request = CreateReviewRequestFactory::new()->make();

    postJson("/api/v1/catalog/reviews-create", $request)
        ->assertOk()
        ->assertJsonPath('data', null);
});

test('DELETE /api/v1/catalog/reviews/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $reviewId = 1;

    $this->mockReviewsReviewsApi()->allows([
        'getReview' => ReviewFactory::new()->makeResponseOne(['id' => $reviewId, 'customer_id' => user()->customerId]),
        'deleteReview' => new EmptyDataResponse(),
    ]);

    deleteJson("/api/v1/catalog/reviews/{$reviewId}")
        ->assertOk();
});

test('DELETE /api/v1/catalog/reviews/{id} forbidden', function () {
    /** @var ApiV1ComponentTestCase $this */
    $reviewId = 1;

    $this->mockReviewsReviewsApi()->allows([
        'getReview' => ReviewFactory::new()->makeResponseOne(['id' => $reviewId, 'customer_id' => user()->customerId + 1]),
        'deleteReview' => new EmptyDataResponse(),
    ]);

    deleteJson("/api/v1/catalog/reviews/{$reviewId}")
        ->assertForbidden();
});

test('POST /api/v1/catalog/reviews:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $reviewId = 1;
    $productId = 1;
    $customerId = 1;

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomers')
        ->andReturn(CustomerFactory::new()->makeResponseSearch(['id' => $customerId, 'avatar' => null]));
    $this->mockReviewsReviewsApi()->allows([
        'searchReviews' => ReviewFactory::new()->makeResponseSearch(['id' => $reviewId, 'product_id' => 1, 'customer_id' => $customerId]),
    ]);

    $request = [
        'filter' => ['product_id' => $productId],
    ];

    postJson('/api/v1/catalog/reviews:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'grade', 'comment', 'created_at']]]);
});
