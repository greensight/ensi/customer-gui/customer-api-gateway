<?php

use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\PimClient\Dto\SearchBrandsRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/catalog/brands:search success', function () {
    $brandId = 10;

    $requestApi = null;
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimBrandsApi()
        ->shouldReceive('searchBrands')
        ->withArgs(function (SearchBrandsRequest $request) use (&$requestApi) {
            $requestApi = $request;

            return true;
        })
        ->andReturn(BrandFactory::new()
            ->makeResponseSearch(
                [
                    ['id' => $brandId],
                    ['id' => $brandId + 1],
                ],
                count: 2
            ));

    postJson('/api/v1/catalog/brands:search')
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'description', 'logo_file']]])
        ->assertJsonPath('data.0.id', $brandId);

    expect($requestApi->getFilter()->is_active)->toBe(true);
});

test('GET /api/v1/catalog/brands/{id} success', function () {
    $brandId = 10;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimBrandsApi()->allows([
        'getBrand' => BrandFactory::new()
            ->makeResponse(['id' => $brandId]),
    ]);

    getJson("/api/v1/catalog/brands/$brandId")
        ->assertOk()
        ->assertJsonPath('data.id', $brandId);
});
