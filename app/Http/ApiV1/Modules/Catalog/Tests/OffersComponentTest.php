<?php

use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Domain\Catalog\Tests\Factories\Cloud\CatalogSearchFactory;
use App\Domain\Catalog\Tests\Factories\Elastic\ElasticOfferFactory;
use App\Domain\Catalog\Tests\Factories\Feed\CloudIntegrationFactory;
use App\Domain\Crm\Tests\Factories\CustomerFavoriteFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\CatalogSearchRequest;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\SearchBrandsRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/offers:search without integration success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $favoriteProductId = 1;
    $productId = 2;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockFeedCloudIntegrationsApi()
        ->shouldReceive('getCloudIntegration')
        ->once()
        ->andReturn(CloudIntegrationFactory::new()->active(false)->makeResponse());

    $basePrice = 1440;
    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffers')
        ->once()
        ->andReturn(ElasticOfferFactory::new()->makeResponseSearch([
            ['price' => $basePrice, 'product_id' => $favoriteProductId],
            ['price' => $basePrice, 'product_id' => $productId],
        ], 2));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/offers:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'product_id', 'name', 'main_image', 'price']]])
        ->assertJsonPath('data.0.price', $basePrice)
        ->assertJsonPath('data.0.product_id', $favoriteProductId)
        ->assertJsonPath('data.0.is_favorite', true)
        ->assertJsonPath('data.1.product_id', $productId)
        ->assertJsonPath('data.1.is_favorite', false);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/offers:search with integration success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $favoriteProductId = 1;
    $productId = 2;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockFeedCloudIntegrationsApi()
        ->shouldReceive('getCloudIntegration')
        ->once()
        ->andReturn(CloudIntegrationFactory::new()->active()->makeResponse());

    $this->mockCloudSdkApi()
        ->shouldReceive('search')
        ->once()
        ->andReturn(
            CatalogSearchFactory::new()
                ->withProducts([(string)$favoriteProductId, (string)$productId])
                ->makeResponse()
        );

    $basePrice = 1440;
    $productIds = null;
    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffers')
        ->once()
        ->withArgs(function (SearchElasticOffersRequest $searchOfferRequest) use (&$productIds) {
            $productIds = $searchOfferRequest->getFilter()->product_id;

            return true;
        })
        ->andReturn(ElasticOfferFactory::new()->makeResponseSearch([
            ['product_id' => $favoriteProductId, 'price' => $basePrice],
            ['product_id' => $productId, 'price' => $basePrice],
        ], 2));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/offers:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'product_id', 'name', 'main_image', 'price']]])
        ->assertJsonPath('data.0.price', $basePrice)
        ->assertJsonPath('data.0.product_id', $favoriteProductId)
        ->assertJsonPath('data.0.is_favorite', true)
        ->assertJsonPath('data.1.product_id', $productId)
        ->assertJsonPath('data.1.is_favorite', false);

    assertEqualsCanonicalizing($productIds, [$favoriteProductId, $productId]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/offers:search with integration brand filter success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockFeedCloudIntegrationsApi()
        ->shouldReceive('getCloudIntegration')
        ->once()
        ->andReturn(CloudIntegrationFactory::new()->active()->makeResponse());

    $brandId = 1;
    $brandName = 'brand-name';
    $brandIds = null;
    $this->mockPimBrandsApi()
        ->shouldReceive('searchBrands')
        ->once()
        ->withArgs(function (SearchBrandsRequest $searchBrandsRequest) use (&$brandIds) {
            $brandIds = $searchBrandsRequest->getFilter()->id;

            return true;
        })
        ->andReturn(BrandFactory::new()->makeResponseSearch([['id' => $brandId, 'name' => $brandName]]));

    $this->mockCloudSdkApi()
        ->shouldReceive('search')
        ->once()
        ->withArgs(function (CatalogSearchRequest $catalogSearchRequest) use (&$brandNames) {
            $brandNames = $catalogSearchRequest->filter->brands;

            return true;
        })
        ->andReturn(
            CatalogSearchFactory::new()->withProducts(["123"])->makeResponse()
        );

    $basePrice = 1440;
    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffers')
        ->once()
        ->andReturn(ElasticOfferFactory::new()->makeResponseSearch([
            ['price' => $basePrice],
            ['price' => $basePrice],
        ], 2));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch());

    $request = [
        'filter' => [
            'name' => 'foo',
            'brand_id' => 1,
        ],
    ];

    postJson('/api/v1/catalog/offers:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'product_id', 'name', 'main_image', 'price']]])
        ->assertJsonPath('data.0.price', $basePrice);

    assertEqualsCanonicalizing($brandIds, [$brandId]);
    assertEqualsCanonicalizing($brandNames, [$brandName]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/offers:search with integration empty response', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockFeedCloudIntegrationsApi()
        ->shouldReceive('getCloudIntegration')
        ->once()
        ->andReturn(CloudIntegrationFactory::new()->active()->makeResponse());

    $this->mockCloudSdkApi()
        ->shouldReceive('search')
        ->once()
        ->andReturn(CatalogSearchFactory::new()->withProducts([])->makeResponse());

    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffers')
        ->never();

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->never();

    $request = ['filter' => ['name' => 'foo']];

    postJson('/api/v1/catalog/offers:search', $request)
        ->assertOk()
        ->assertJsonCount(0, 'data');
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/offers/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $basePrice = 12270;
    $productId = 1;
    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchOneElasticOffer')
        ->once()
        ->andReturn(ElasticOfferFactory::new()->makeResponse([
            'price' => $basePrice,
            'product_id' => $productId,
        ]));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $productId]]));

    getJson("/api/v1/catalog/offers/1")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'product_id', 'name', 'price', 'main_image']])
        ->assertJsonPath('data.price', $basePrice)
        ->assertJsonPath('data.is_favorite', true);

})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/offers/{id} success all include', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */

    $elasticOffer = ElasticOfferFactory::new()
        ->withDiscount()
        ->withCategories()
        ->withBrand()
        ->withImages()
        ->withAttributes()
        ->withGluing()
        ->withNameplates()
        ->makeResponse();

    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchOneElasticOffer')
        ->once()
        ->andReturn($elasticOffer);

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch());

    $elasticOfferData = $elasticOffer->getData();

    getJson("/api/v1/catalog/offers/1?include=discount,categories,brand,images,attributes,gluing")
        ->assertOk()
        ->assertJsonStructure(['data' => ['discount', 'categories', 'brand', 'images', 'attributes', 'gluing']])
        ->assertJsonPath('data.discount.value', $elasticOfferData->getDiscount()->getValue())
        ->assertJsonPath('data.discount.value_type', $elasticOfferData->getDiscount()->getValueType())
        ->assertJsonPath('data.categories.0.id', current($elasticOfferData->getCategories())->getId())
        ->assertJsonPath('data.brand.id', $elasticOfferData->getBrand()->getId())
        ->assertJsonPath('data.images.0.id', current($elasticOfferData->getImages())->getId())
        ->assertJsonPath('data.attributes.0.name', current($elasticOfferData->getAttributes())->getName())
        ->assertJsonPath('data.gluing.0.id', current($elasticOfferData->getGluing())->getId())
        ->assertJsonPath('data.nameplates.0.id', current($elasticOfferData->getNameplates())->getId());
})->with(FakerProvider::$optionalDataset);
