<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\ElasticOfferResponse;
use Ensi\CatalogCacheClient\Dto\RequestBodyPagination;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\CatalogCacheClient\Dto\SearchOneElasticOffersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ElasticOffersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        Request $request,
        protected readonly ElasticOffersApi $offersApi,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteAction,
    ) {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return SearchElasticOffersRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function forcedFilters(): array
    {
        return ['allow_publish' => true, 'gluing_is_active' => true];
    }

    protected function prepareFilters(array $filters): array
    {
        if (isset($filters['id'])) {
            $filters['offer_id'] = $filters['id'];
            unset($filters['id']);
        }

        return $filters;
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ElasticOfferResponse
    {
        $request = new SearchOneElasticOffersRequest();
        $request->setFilter((object)['offer_id' => $id, 'gluing_is_active' => true]);
        $request->setInclude($this->getInclude());

        return $this->offersApi->searchOneElasticOffer($request);
    }

    protected function search($request): SearchElasticOffersResponse
    {
        return $this->offersApi->searchElasticOffers($request);
    }

    /**
     * @param ElasticOfferResponse $response
     */
    protected function convertFindToItem($response): ?ElasticOfferData
    {
        return $this->convertToProductsData([$response->getData()])->first();
    }

    /**
     * @param SearchElasticOffersResponse $response
     * @return Collection<ElasticOfferData>
     */
    protected function convertGetToItems($response): Collection
    {
        return $this->convertToProductsData($response->getData());
    }

    protected function convertToProductsData(array $offers): Collection
    {
        $offers = new Collection($offers);

        $offers = $offers->map(fn (ElasticOffer $offer) => new ElasticOfferData($offer));

        return $this->enrichFavoriteAction->execute($offers);
    }

    protected function prepareSort(array $sorts): array
    {
        $fixed = [];
        foreach ($sorts as $sort) {
            $fixed[] = preg_replace(
                ['/^(-?)name$/', '/^(-?)id$/',],
                ['$1name_sort', '$1product_id',],
                $sort
            );
        }

        return $fixed;
    }
}
