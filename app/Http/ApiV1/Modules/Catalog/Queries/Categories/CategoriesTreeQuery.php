<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Categories;

use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CategoriesTreeFilter;
use Ensi\PimClient\Dto\CategoriesTreeItem;
use Ensi\PimClient\Dto\CategoriesTreeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CategoriesTreeQuery
{
    public function __construct(
        protected Request $httpRequest,
        protected readonly CategoriesApi $api
    ) {
    }

    /**
     * @return Collection<CategoriesTreeItem>
     * @throws ApiException
     */
    public function get(): Collection
    {
        $request = new CategoriesTreeRequest();
        $parentId = $this->httpRequest->get('parent_id');

        $filter = new CategoriesTreeFilter();
        $filter->setIsActive(true);

        if ($parentId) {
            $filter->setRootId($parentId);
        }

        $request->setFilter($filter);

        return collect($this->api->getCategoriesTree($request)->getData());
    }
}
