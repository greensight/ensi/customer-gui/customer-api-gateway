<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Reviews;

use App\Domain\Catalog\Data\Reviews\ReviewData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination as RequestBodyPaginationCustomers;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\ApiException;
use Ensi\ReviewsClient\Dto\RequestBodyPagination;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;
use Ensi\ReviewsClient\Dto\SearchReviewsRequest;
use Ensi\ReviewsClient\Dto\SearchReviewsResponse;
use Illuminate\Http\Request;

class ReviewsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    protected bool $loadCustomers = false;

    public function __construct(
        Request $request,
        protected readonly ReviewsApi $reviewsApi,
        protected readonly CustomersApi $customersApi
    ) {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return SearchReviewsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchReviewsResponse
    {
        return $this->reviewsApi->searchReviews($request);
    }

    protected function convertGetToItems(SearchReviewsResponse $response): array
    {
        $reviews = collect($response->getData());
        $customerIds = $reviews->pluck('customer_id')->all();

        $customers = collect();

        if ($this->loadCustomers && !empty($customerIds)) {
            $request = new SearchCustomersRequest();
            $request->setFilter((object) ['id' => $customerIds]);
            $request->setPagination((new RequestBodyPaginationCustomers())->setLimit(-1)->setType(PaginationTypeEnum::CURSOR));

            $customers = collect($this->customersApi->searchCustomers($request)->getData())->keyBy('id');
        }

        $reviewsData = [];
        foreach ($reviews as $review) {
            $customer = $customers->get($review->getCustomerId());

            $reviewsData[] = new ReviewData($review, $customer);
        }

        return $reviewsData;
    }

    protected function forcedFilters(): array
    {
        return ['status_id' => ReviewStatusEnum::PUBLISHED];
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'customers':
                    $this->loadCustomers = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
