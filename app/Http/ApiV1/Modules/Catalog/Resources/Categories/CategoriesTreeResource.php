<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Categories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\CategoriesTreeItem;

/**
 * @mixin CategoriesTreeItem
 */
class CategoriesTreeResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'children' => $this->whenNotNull($this->getChildren()),
        ];
    }
}
