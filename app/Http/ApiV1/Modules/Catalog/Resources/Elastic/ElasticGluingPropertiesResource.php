<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\GluingProperty;

/**
 * @mixin GluingProperty
 */
class ElasticGluingPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'prop_type' => $this->getPropType(),
            'prop_name' => $this->getPropName(),
            'prop_code' => $this->getPropCode(),
            'value_value' => $this->getValueValue(),
            'value_name' => $this->getValueName(),
        ];
    }
}
