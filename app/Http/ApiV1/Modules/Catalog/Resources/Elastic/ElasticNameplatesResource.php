<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Elastic;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ElasticNameplate;

/** @mixin ElasticNameplate */
class ElasticNameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'background_color' => $this->getBackgroundColor(),
            'text_color' => $this->getTextColor(),
        ];
    }
}
