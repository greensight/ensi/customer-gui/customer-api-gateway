<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\Nameplate;

/** @mixin Nameplate */
class ProductNameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'background_color' => $this->getBackgroundColor(),
            'text_color' => $this->getTextColor(),
        ];
    }
}
