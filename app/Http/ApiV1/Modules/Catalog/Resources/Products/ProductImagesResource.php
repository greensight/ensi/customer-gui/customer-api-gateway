<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\ProductImage;
use Ensi\CatalogCacheClient\Dto\ElasticImage;

/**
 * @mixin ElasticImage|ProductImage
 */
class ProductImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'sort' => $this->getSort(),
            'name' => $this->getName(),
            'url' => $this->getUrl(),
        ];
    }
}
