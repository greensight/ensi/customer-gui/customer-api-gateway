<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Reviews;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomersClient\Dto\Customer;

/**
 * @mixin Customer
 */
class ReviewCustomersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
            'middle_name' => $this->getMiddleName(),
            'full_name' => $this->getFullName(),
            'avatar' => $this->fileUrl($this->getAvatar()),
        ];
    }
}
