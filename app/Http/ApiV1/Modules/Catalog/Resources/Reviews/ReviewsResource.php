<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Reviews;

use App\Domain\Catalog\Data\Reviews\ReviewData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ReviewData
 */
class ReviewsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->review->getId(),
            'grade' => $this->review->getGrade(),
            'product_id' => $this->review->getProductId(),
            'comment' => $this->review->getComment(),
            'created_at' => $this->dateTimeToIso($this->review->getCreatedAt()),

            'customer' => ReviewCustomersResource::make($this->whenNotNull($this->customer)),
        ];
    }
}
