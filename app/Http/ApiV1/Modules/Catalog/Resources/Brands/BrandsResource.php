<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Brands;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Brand;

/**
 * @mixin Brand
 */
class BrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'description' => $this->getDescription(),
            'logo_file' => $this->getLogoFile()?->getUrl() ?? $this->getLogoUrl(),
        ];
    }
}
