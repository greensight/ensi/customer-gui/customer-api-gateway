<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Categories;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CategoriesTreeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => ['sometimes', 'integer'],
        ];
    }
}
