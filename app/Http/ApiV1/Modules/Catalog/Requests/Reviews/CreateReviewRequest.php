<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Reviews;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateReviewRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'product_id' => ['required', 'integer'],
            'comment' => ['nullable', 'string'],
            'grade' => ['required', 'integer'],
        ];
    }
}
