<?php

namespace App\Http\ApiV1\Modules\Crm\Queries;

use App\Domain\Crm\Data\FavoriteOfferData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\RequestBodyPagination as CCRequestBodyPagination;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\CustomerFavorite;
use Ensi\CrmClient\Dto\RequestBodyPagination;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FavoriteOffersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    /** @var Collection<ElasticOffer> */
    protected Collection $offers;

    public function __construct(
        Request $httpRequest,
        protected readonly CustomerFavoritesApi $favoritesApi,
        protected readonly ElasticOffersApi $offersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchCustomerFavoritesRequest::class;
    }

    protected function forcedFilters(): array
    {
        return ['customer_id' => user()->customerId];
    }

    protected function search($request): SearchCustomerFavoritesResponse
    {
        return $this->favoritesApi->searchCustomerFavorite($request);
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $favorites): Collection
    {
        /** @var Collection|CustomerFavorite[] $favorites */
        $favorites = collect($favorites);

        $productIds = $favorites->pluck('product_id')->all();
        $this->loadOffers($productIds)->wait();

        $favoriteOffersData = collect();
        foreach ($favorites as $favorite) {
            /** @var ElasticOffer|null $offer */
            $offer = $this->offers->get($favorite->getProductId());
            if (!$offer) {
                continue;
            }

            $favoriteOffersData->add(new FavoriteOfferData($offer));
        }

        return $favoriteOffersData;
    }

    protected function loadOffers(array $productIds): PromiseInterface
    {
        $request = new SearchElasticOffersRequest();
        $request->setFilter((object)['product_id' => $productIds]);
        $request->setPagination((new CCRequestBodyPagination())->setLimit(count($productIds)));
        $request->setInclude($this->getInclude());

        return $this->offersApi->searchElasticOffersAsync($request)
            ->then(function (SearchElasticOffersResponse $response) {
                $this->offers = collect($response->getData())->keyBy('product_id');
            });
    }
}
