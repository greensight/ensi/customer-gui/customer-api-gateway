<?php

use App\Domain\Auth\Models\User;
use App\Domain\Catalog\Tests\Factories\Elastic\ElasticOfferFactory;
use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Crm\Tests\Factories\CustomerFavoriteFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest as CCSearchOffersRequest;
use Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\EmptyDataResponse;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\OffersClient\Dto\SearchOffersRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/catalog/favorite-products:add 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $offerId = 1;
    $productId = 2;

    $user = User::factory()->make();
    $this->actingAs($user);

    $requestApi = null;
    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('createCustomerFavorites')
        ->once()
        ->withArgs(function (CreateCustomerFavoritesRequest $request) use (&$requestApi) {
            $requestApi = $request;

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    $requestOffersApi = null;
    $this->mockOffersOffersApi()
        ->shouldReceive('searchOffers')
        ->once()
        ->withArgs(function (SearchOffersRequest $request) use (&$requestOffersApi) {
            $requestOffersApi = $request;

            return true;
        })
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));

    postJson('/api/v1/catalog/favorite-products:add', ['offer_ids' => [$offerId]])
        ->assertStatus(200);

    expect($requestApi->getCustomerId())->toBe($user->customerId)
        ->and($requestApi->getProductIds())->toBe([$productId])
        ->and($requestOffersApi->getFilter()->id)->toBe([$offerId]);
});

test('DELETE /api/v1/catalog/favorite-products 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $offerId = 1;
    $productId = 2;

    $user = User::factory()->make();
    $this->actingAs($user);

    $requestApi = null;
    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('deleteCustomerFavorites')
        ->once()
        ->withArgs(function (DeleteCustomerFavoritesRequest $request) use (&$requestApi) {
            $requestApi = $request;

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    $requestOffersApi = null;
    $this->mockOffersOffersApi()
        ->shouldReceive('searchOffers')
        ->once()
        ->withArgs(function (SearchOffersRequest $request) use (&$requestOffersApi) {
            $requestOffersApi = $request;

            return true;
        })
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));

    deleteJson('/api/v1/catalog/favorite-products', ['offer_ids' => [$offerId]])
        ->assertStatus(200);

    expect($requestApi->getCustomerId())->toBe($user->customerId)
        ->and($requestApi->getProductIds())->toBe([$productId])
        ->and($requestOffersApi->getFilter()->id)->toBe([$offerId]);
});

test('POST /api/v1/catalog/favorite-products:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $offerId = 1;
    $productId = 2;

    $user = User::factory()->make();
    $this->actingAs($user);

    $requestApi = null;
    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->withArgs(function (SearchCustomerFavoritesRequest $request) use (&$requestApi) {
            $requestApi = $request;

            return true;
        })
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $productId]]));

    $requestOffersApi = null;
    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffersAsync')
        ->once()
        ->withArgs(function (CCSearchOffersRequest $request) use (&$requestOffersApi) {
            $requestOffersApi = $request;

            return true;
        })
        ->andReturn(PromiseFactory::make(ElasticOfferFactory::new()->makeResponseSearch([[
            'id' => $offerId,
            'product_id' => $productId,
        ]])));

    postJson('/api/v1/catalog/favorite-products:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $offerId)
        ->assertJsonPath('data.0.is_favorite', true);

    expect($requestApi->getFilter()->customer_id)->toBe($user->customerId)
        ->and($requestOffersApi->getFilter()->product_id)->toBe([$productId]);
});
