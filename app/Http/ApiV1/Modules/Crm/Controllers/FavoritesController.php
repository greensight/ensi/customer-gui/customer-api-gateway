<?php

namespace App\Http\ApiV1\Modules\Crm\Controllers;

use App\Domain\Crm\Actions\Favorites\CreateCustomerFavoritesAction;
use App\Domain\Crm\Actions\Favorites\DeleteCustomerFavoritesAction;
use App\Http\ApiV1\Modules\Crm\Queries\FavoriteOffersQuery;
use App\Http\ApiV1\Modules\Crm\Requests\CreateCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Crm\Requests\DeleteCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Crm\Resources\FavoritesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class FavoritesController
{
    public function massAdd(CreateCustomerFavoritesRequest $request, CreateCustomerFavoritesAction $action): Responsable
    {
        $action->execute($request->getOfferIds());

        return new EmptyResource();
    }

    public function massDelete(DeleteCustomerFavoritesRequest $request, DeleteCustomerFavoritesAction $action): Responsable
    {
        $action->execute($request->getOfferIds());

        return new EmptyResource();
    }

    public function search(FavoriteOffersQuery $query): Responsable
    {
        return FavoritesResource::collectPage($query->get());
    }
}
