<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Domain\Auth\Actions\Data\LoginRequestData;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class LoginRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }

    public function convertToObject(): LoginRequestData
    {
        return new LoginRequestData(
            $this->validated('login'),
            $this->validated('password'),
        );
    }
}
