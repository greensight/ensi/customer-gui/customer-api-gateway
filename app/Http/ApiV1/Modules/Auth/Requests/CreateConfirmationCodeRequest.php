<?php

namespace App\Http\ApiV1\Modules\Auth\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;

class CreateConfirmationCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'phone' => ['required', 'string', new PhoneRule()],
        ];
    }
}
