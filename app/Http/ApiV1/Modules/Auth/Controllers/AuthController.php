<?php

namespace App\Http\ApiV1\Modules\Auth\Controllers;

use App\Domain\Auth\Actions\LoginAction;
use App\Domain\Auth\Actions\LogoutAction;
use App\Domain\Auth\Actions\RefreshAction;
use App\Domain\Auth\Actions\RegisterUserAction;
use App\Http\ApiV1\Modules\Auth\Requests\LoginByCodeRequest;
use App\Http\ApiV1\Modules\Auth\Requests\LoginRequest;
use App\Http\ApiV1\Modules\Auth\Requests\RefreshRequest;
use App\Http\ApiV1\Modules\Auth\Requests\RegisterRequest;
use App\Http\ApiV1\Modules\Auth\Resources\TokensResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;

class AuthController
{
    public function register(RegisterRequest $request, RegisterUserAction $action): EmptyResource
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function login(LoginRequest $request, LoginAction $action): TokensResource
    {
        return TokensResource::make($action->execute($request->convertToObject()));
    }

    public function loginByCode(LoginByCodeRequest $request, LoginAction $action): TokensResource
    {
        return TokensResource::make($action->execute($request->convertToObject()));
    }

    public function logout(LogoutAction $action, Request $request): EmptyResource
    {
        $action->execute($request->user()->getRememberToken());

        return new EmptyResource();
    }

    public function refresh(RefreshRequest $request, RefreshAction $action): TokensResource
    {
        return TokensResource::make($action->execute($request->getRefreshToken()));
    }
}
