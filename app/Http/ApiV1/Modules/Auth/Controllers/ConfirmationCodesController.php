<?php

namespace App\Http\ApiV1\Modules\Auth\Controllers;

use App\Domain\Auth\Actions\CreateConfirmationCodeAction;
use App\Http\ApiV1\Modules\Auth\Requests\CreateConfirmationCodeRequest;
use App\Http\ApiV1\Modules\Auth\Resources\ConfirmationCodesResource;

class ConfirmationCodesController
{
    public function create(CreateConfirmationCodeRequest $request, CreateConfirmationCodeAction $action): ConfirmationCodesResource
    {
        return ConfirmationCodesResource::make($action->execute($request->validated()));
    }
}
