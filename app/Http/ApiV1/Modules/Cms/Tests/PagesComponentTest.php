<?php

namespace App\Http\ApiV1\Modules\Cms\Tests;

use App\Domain\Cms\Tests\Factories\Pages\PageFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/pages:search success', function () {
    $pageId = 1;
    $pageName = 'Important news';
    $slug = 'important-news';

    $this->mockCmsPagesApi()->allows([
        'searchPages' => PageFactory::new()->makeResponseSearch([[
            'id' => $pageId,
            'name' => $pageName,
            'slug' => $slug,
        ]]),
    ]);

    postJson('/api/v1/pages:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $pageId)
        ->assertJsonPath('data.0.name', $pageName)
        ->assertJsonPath('data.0.url', $slug);
});
