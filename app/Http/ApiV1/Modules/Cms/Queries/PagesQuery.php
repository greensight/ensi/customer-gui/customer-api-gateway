<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchPagesRequest;
use Ensi\CmsClient\Dto\SearchPagesResponse;
use Illuminate\Http\Request;

class PagesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected PagesApi $pagesApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchPagesRequest::class;
    }

    protected function fillFilters($request): void
    {
        $request->setFilter(
            array_merge(['is_visible' => true], (array) $this->httpRequest->get('filter'))
        );
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPagesResponse
    {
        return $this->pagesApi->searchPages($request);
    }
}
