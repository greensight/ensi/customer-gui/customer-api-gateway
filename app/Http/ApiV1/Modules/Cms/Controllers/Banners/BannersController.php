<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers\Banners;

use App\Http\ApiV1\Modules\Cms\Queries\Banners\BannersQuery;
use App\Http\ApiV1\Modules\Cms\Resources\Banners\BannersResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BannersController
{
    public function search(BannersQuery $query): AnonymousResourceCollection
    {
        return BannersResource::collectPage($query->get());
    }
}
