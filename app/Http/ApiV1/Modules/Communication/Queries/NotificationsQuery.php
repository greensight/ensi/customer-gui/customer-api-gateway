<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\RequestBodyPagination;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationsRequest;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationsResponse;
use Illuminate\Http\Request;

class NotificationsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected readonly NotificationsApi $api)
    {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return SearchNotificationsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function search($request): SearchNotificationsResponse
    {
        return $this->api->searchNotifications($request);
    }

    protected function forcedFilters(): array
    {
        return ['customer_id' => user()->customerId, 'channels_like' => NotificationChannelEnum::STOREFRONT];
    }
}
