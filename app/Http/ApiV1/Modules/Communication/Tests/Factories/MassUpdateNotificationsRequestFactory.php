<?php

namespace App\Http\ApiV1\Modules\Communication\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class MassUpdateNotificationsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'notification_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), min: 1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
