<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\Notification;

/**
 * @mixin Notification
 */
class NotificationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'theme' => $this->getTheme(),
            'text' => $this->getText(),
            'is_viewed' => $this->getIsViewed(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
        ];
    }
}
