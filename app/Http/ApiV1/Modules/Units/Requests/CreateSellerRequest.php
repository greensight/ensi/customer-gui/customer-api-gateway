<?php

namespace App\Http\ApiV1\Modules\Units\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateSellerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'legal_name' => ['required', 'string'],
            ...self::addressField('legal_address'),
            ...self::addressField('fact_address'),
            'inn' => ['required', 'string'],
            'kpp' => ['required', 'string'],
            'payment_account' => ['required', 'string'],
            'correspondent_account' => ['required', 'string'],
            'bank' => ['required', 'string'],
            ...self::addressField('bank_address'),
            'bank_bik' => ['required', 'string'],
            'site' => ['nullable', 'string'],
            'info' => ['required', 'string'],
        ];
    }

    protected static function addressField(string $prefix): array
    {
        return self::nestedRules($prefix, self::addressRules());
    }

    protected static function addressRules(): array
    {
        return [
            "address_string" => ['required', 'string'],
            "country_code" => ['nullable', 'string'],
            "post_index" => ['nullable', 'string'],
            "region" => ['nullable', 'string'],
            "region_guid" => ['nullable', 'string'],
            "area" => ['nullable', 'string'],
            "area_guid" => ['nullable', 'string'],
            "city" => ['nullable', 'string'],
            "city_guid" => ['nullable', 'string'],
            "street" => ['nullable', 'string'],
            "house" => ['nullable', 'string'],
            "block" => ['nullable', 'string'],
            "flat" => ['nullable', 'string'],
            "floor" => ['nullable', 'string'],
            "porch" => ['nullable', 'string'],
            "intercom" => ['nullable', 'string'],
            "geo_lat" => ['nullable', 'string'],
            "geo_lon" => ['nullable', 'string'],
        ];
    }
}
