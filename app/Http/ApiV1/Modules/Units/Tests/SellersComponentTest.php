<?php

use App\Domain\Units\Tests\Bu\Factories\SellerFactory;
use App\Http\ApiV1\Modules\Units\Tests\Factories\SellerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/sellers 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $this->mockBuSellersApi()
        ->shouldReceive('createSeller')
        ->andReturn(SellerFactory::new()->makeResponse());

    $request = SellerRequestFactory::new()->make();

    postJson('/api/v1/sellers', $request)
        ->assertOk();
});
