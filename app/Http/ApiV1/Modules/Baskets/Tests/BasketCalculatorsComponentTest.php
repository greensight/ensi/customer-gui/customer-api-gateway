<?php

use App\Domain\Crm\Tests\Factories\CustomerFavoriteFactory;
use App\Domain\Orders\Tests\Baskets\Factories\BasketCalculateFactory;
use App\Domain\Orders\Tests\Baskets\Factories\CalculateBasketItemFactory;
use App\Http\ApiV1\Modules\Baskets\Tests\Factories\SetBasketPromoCodeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');


test('POST /api/v1/baskets:current 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $offerId = 1;
    $favoriteOfferId = 2;
    $productId = 10;
    $favoriteProductId = 20;

    $item = CalculateBasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);
    $favoriteItem = CalculateBasketItemFactory::new()->make(['offer_id' => $favoriteOfferId, 'product_id' => $favoriteProductId]);

    $this->mockBasketsCalculatorsApi()
        ->shouldReceive('searchBasketCalculate')
        ->once()
        ->andReturn(BasketCalculateFactory::new()->withItems($item)->withItems($favoriteItem)->makeResponse());

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    postJson('/api/v1/baskets:current')
        ->assertStatus(200)
        ->assertJsonPath('data.items.0.id', $offerId)
        ->assertJsonPath('data.items.0.is_favorite', false)
        ->assertJsonPath('data.items.1.id', $favoriteOfferId)
        ->assertJsonPath('data.items.1.is_favorite', true);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/baskets:set-promo 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentTestCase $this */

    $offerId = 1;
    $favoriteOfferId = 2;
    $productId = 10;
    $favoriteProductId = 20;

    $item = CalculateBasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);
    $favoriteItem = CalculateBasketItemFactory::new()->make(['offer_id' => $favoriteOfferId, 'product_id' => $favoriteProductId]);

    $this->mockBasketsCalculatorsApi()
        ->shouldReceive('setBasketCalculatePromoCode')
        ->once()
        ->andReturn(BasketCalculateFactory::new()
            ->withPromoCodeStatus()
            ->withItems($item)
            ->withItems($favoriteItem)
            ->makeResponse());

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    $request = SetBasketPromoCodeRequestFactory::new()->make();

    postJson('/api/v1/baskets:set-promo', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.items.0.id', $offerId)
        ->assertJsonPath('data.items.0.is_favorite', false)
        ->assertJsonPath('data.items.1.id', $favoriteOfferId)
        ->assertJsonPath('data.items.1.is_favorite', true);
})->with(FakerProvider::$optionalDataset);
