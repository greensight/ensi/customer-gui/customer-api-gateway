<?php

namespace App\Http\ApiV1\Modules\Baskets\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class SetBasketPromoCodeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'promo_code' => $this->faker->nullable()->word(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
