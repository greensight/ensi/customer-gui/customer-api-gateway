<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Orders\Actions\Baskets\DeleteBasketAction;
use App\Domain\Orders\Actions\Baskets\SetBasketItemsAction;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketItemsRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class BasketsController
{
    public function setItems(SetBasketItemsRequest $request, SetBasketItemsAction $action): Responsable
    {
        $action->execute($request->getItems());

        return new EmptyResource();
    }

    public function delete(DeleteBasketAction $action): Responsable
    {
        $action->execute();

        return new EmptyResource();
    }
}
