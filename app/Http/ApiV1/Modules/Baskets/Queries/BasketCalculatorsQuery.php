<?php

namespace App\Http\ApiV1\Modules\Baskets\Queries;

use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Domain\Orders\Data\Baskets\CalculateBasketData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use Ensi\BasketsClient\Api\CalculatorsBasketsApi;
use Ensi\BasketsClient\Dto\SearchBasketCalculateRequest;
use Illuminate\Http\Request;

class BasketCalculatorsQuery extends QueryBuilder
{
    protected array $include = ['items', 'brand'];

    public function __construct(
        Request $request,
        protected readonly CalculatorsBasketsApi $basketsApi,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteEntityAction,
    ) {
        parent::__construct($request);
    }

    public function current(): CalculateBasketData
    {
        $request = new SearchBasketCalculateRequest();
        $request->setCustomerId(user()->customerId);

        $this->fillInclude($request);

        $basket = $this->basketsApi->searchBasketCalculate($request)->getData();

        $basketData = CalculateBasketData::make($basket);
        $this->enrichFavoriteEntityAction->execute($basketData->getItems());

        return $basketData;
    }
}
