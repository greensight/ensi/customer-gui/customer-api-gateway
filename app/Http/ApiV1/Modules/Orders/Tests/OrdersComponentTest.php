<?php

use App\Domain\Catalog\Tests\Factories\Elastic\ElasticOfferFactory;
use App\Domain\Crm\Tests\Factories\CustomerFavoriteFactory;
use App\Domain\Logistic\Tests\Factories\DeliveryPriceFactory;
use App\Domain\Orders\Tests\Baskets\Factories\BasketCalculateFactory;
use App\Domain\Orders\Tests\Baskets\Factories\CalculateBasketItemFactory;
use App\Domain\Orders\Tests\Oms\Factories\OrderCommitFactory;
use App\Domain\Orders\Tests\Oms\Factories\OrderFactory;
use App\Domain\Orders\Tests\Oms\Factories\OrderItemFactory;
use App\Domain\Units\Tests\Bu\Factories\StoreFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\GetCheckoutDataRequestFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Factories\OrderCommitRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\OmsClient\Dto\OrderCommitRequest;
use GuzzleHttp\Promise\Create;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/checkout/order 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $offerId = 2;
    $productId = 3;
    $item = CalculateBasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);

    $request = OrderCommitRequestFactory::new()->make();

    $this->mockBasketsCalculatorsApi()->allows([
        'searchBasketCalculateAsync' => PromiseFactory::make(BasketCalculateFactory::new()->withItems($item)->makeResponse()),
    ]);
    $this->mockBuStoresApi()->allows([
        'searchStoresAsync' => PromiseFactory::make(StoreFactory::new()->makeResponseSearch()),
    ]);
    $this->mockDeliveryPricesApi()->allows([
        'searchDeliveryPriceAsync' => PromiseFactory::make(DeliveryPriceFactory::new()->makeResponse()),
    ]);

    $this->mockOrdersOrdersApi()->allows([
        'commitOrder' => OrderCommitFactory::new()->makeResponse(['order_id' => $orderId]),
    ]);

    postJson('/api/v1/checkout/order', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.order_id', $orderId);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/checkout/order - undefined delivery price - 400', function () {
    /** @var ApiV1ComponentTestCase $this */
    $offerId = 2;
    $productId = 3;
    $item = CalculateBasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);


    $this->mockBasketsCalculatorsApi()->allows([
        'searchBasketCalculateAsync' => PromiseFactory::make(BasketCalculateFactory::new()->withItems($item)->makeResponse()),
    ]);
    $this->mockBuStoresApi()->allows([
        'searchStoresAsync' => PromiseFactory::make(StoreFactory::new()->makeResponseSearch()),
    ]);
    $this->mockDeliveryPricesApi()
        ->shouldReceive('searchDeliveryPriceAsync')
        ->once()
        ->andReturn(Create::rejectionFor(new ApiException(code: 404)));

    postJson('/api/v1/checkout/order', OrderCommitRequestFactory::new()->setDeliveryMethod(DeliveryMethodEnum::DELIVERY)->make())
        ->assertStatus(400);
});

test('POST /api/v1/checkout/order - without delivery - 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $offerId = 2;
    $productId = 3;
    $item = CalculateBasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);

    $this->mockBasketsCalculatorsApi()->allows([
        'searchBasketCalculateAsync' => PromiseFactory::make(BasketCalculateFactory::new()->withItems($item)->makeResponse()),
    ]);
    $this->mockBuStoresApi()->allows([
        'searchStoresAsync' => PromiseFactory::make(StoreFactory::new()->makeResponseSearch()),
    ]);

    $commitRequest = null;
    $this->mockOrdersOrdersApi()
        ->shouldReceive('commitOrder')
        ->once()
        ->withArgs(function (OrderCommitRequest $_request) use (&$commitRequest) {
            $commitRequest = $_request;

            return true;
        })
        ->andReturn(OrderCommitFactory::new()->makeResponse());

    postJson('/api/v1/checkout/order', OrderCommitRequestFactory::new()->setDeliveryMethod(DeliveryMethodEnum::PICKUP)->make())
        ->assertStatus(200);

    assertEquals(0, $commitRequest->getDeliveryPrice());
});

test('POST /api/v1/checkout/typ/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $offerId = 2;
    $favoriteOfferId = 3;
    $productId = 20;
    $favoriteProductId = 30;

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $favoriteOrderItem = OrderItemFactory::new()->make(['offer_id' => $favoriteOfferId]);

    $this->mockOrdersOrdersApi()
        ->shouldReceive('searchOrders')
        ->once()
        ->andReturn(
            OrderFactory::new()
                ->withItems($orderItem)
                ->withItems($favoriteOrderItem)
                ->makeResponseSearch([['id' => $orderId]])
        );

    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffersAsync')
        ->once()
        ->andReturn(PromiseFactory::make(ElasticOfferFactory::new()->makeResponseSearch([
            [
                'id' => $offerId,
                'product_id' => $productId,
            ],
            [
                'id' => $favoriteOfferId,
                'product_id' => $favoriteProductId,
            ],
        ])));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    getJson("/api/v1/checkout/typ/{$orderId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'client_comment']])
        ->assertJsonPath('data.id', $orderId)
        ->assertJsonPath('data.items.0.offer_id', $offerId)
        ->assertJsonPath('data.items.0.is_favorite', false)
        ->assertJsonPath('data.items.1.offer_id', $favoriteOfferId)
        ->assertJsonPath('data.items.1.is_favorite', true);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/lk/orders:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;

    $this->mockOrdersOrdersApi()->allows([
        'searchOrders' => OrderFactory::new()->makeResponseSearch([['id' => $orderId]]),
    ]);

    postJson("/api/v1/lk/orders:search")
        ->assertOk()
        ->assertJsonPath('data.0.id', $orderId);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/lk/orders/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $orderId = 1;
    $offerId = 2;
    $favoriteOfferId = 3;
    $productId = 20;
    $favoriteProductId = 30;

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $favoriteOrderItem = OrderItemFactory::new()->make(['offer_id' => $favoriteOfferId]);


    $this->mockOrdersOrdersApi()
        ->shouldReceive('searchOrders')
        ->once()
        ->andReturn(
            OrderFactory::new()
                ->withItems($orderItem)
                ->withItems($favoriteOrderItem)
                ->makeResponseSearch([['id' => $orderId]])
        );

    $this->mockCatalogCacheOffersApi()
        ->shouldReceive('searchElasticOffersAsync')
        ->once()
        ->andReturn(PromiseFactory::make(ElasticOfferFactory::new()->makeResponseSearch([
            [
                'id' => $offerId,
                'product_id' => $productId,
            ],
            [
                'id' => $favoriteOfferId,
                'product_id' => $favoriteProductId,
            ],
        ])));

    $this->mockCustomersCustomerFavoritesApi()
        ->shouldReceive('searchCustomerFavorite')
        ->once()
        ->andReturn(CustomerFavoriteFactory::new()->makeResponseSearch([['product_id' => $favoriteProductId]]));

    getJson("/api/v1/lk/orders/{$orderId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'client_comment']])
        ->assertJsonPath('data.id', $orderId)
        ->assertJsonPath('data.order_items.0.offer_id', $offerId)
        ->assertJsonPath('data.order_items.0.is_favorite', false)
        ->assertJsonPath('data.order_items.1.offer_id', $favoriteOfferId)
        ->assertJsonPath('data.order_items.1.is_favorite', true);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/checkout/data 200', function () {
    $request = GetCheckoutDataRequestFactory::new()->make();

    $this->mockDeliveryPricesApi()->allows([
        'checkoutDeliveryPrices' => DeliveryPriceFactory::new()->makeResponseSearch(),
    ]);

    postJson('/api/v1/checkout/data', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['payment_systems' => [], 'delivery_methods' => []]]);
});
