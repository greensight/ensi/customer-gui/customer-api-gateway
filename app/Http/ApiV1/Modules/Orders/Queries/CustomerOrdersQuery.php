<?php

namespace App\Http\ApiV1\Modules\Orders\Queries;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\RequestBodyPagination as CatalogCacheRequestBodyPagination;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersResponse;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\RequestBodyPagination;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Ensi\OmsClient\Dto\SearchOrdersResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomerOrdersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    protected bool $loadImages = false;

    /** @var Collection<ElasticOffer> */
    protected Collection $offers;

    public function __construct(
        protected readonly OrdersApi $ordersApi,
        protected readonly ElasticOffersApi $offersApi,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteAction,
        Request $request
    ) {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return SearchOrdersRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    public function findOrFail($id): OrderData
    {
        $request = new SearchOrdersRequest();
        $request->setInclude(['items']);
        $request->setFilter((object)[
            'id' => $id,
            'customer_id' => user()->customerId,
        ]);
        $response = $this->ordersApi->searchOrders($request);

        if (empty($response->getData())) {
            throw new NotFoundHttpException();
        }

        return $this->convertFindToItem($response);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchOrdersResponse
    {
        /** @var SearchOrdersRequest $request */
        $request->setFilter((object)[
            'customer_id' => user()->customerId,
        ]);

        return $this->ordersApi->searchOrders($request);
    }

    protected function convertFindToItem($response): OrderData
    {
        return current($this->convertArray($response->getData()));
    }

    protected function convertArray(array $orders): array
    {
        /** @var Collection|Order[] $orders */
        $orders = collect($orders);

        $offerIds = $orders->pluck('items')->collapse()->pluck('offer_id')->all();
        $this->loadOffers($offerIds)->wait();

        $offers = $this->offers->map(fn (ElasticOffer $offer) => new ElasticOfferData($offer));
        $offers = $this->enrichFavoriteAction->execute($offers);

        return OrderData::makeArray($orders, $offers);
    }

    protected function loadOffers(array $ids): PromiseInterface
    {
        $request = new SearchElasticOffersRequest();
        $request->setFilter((object)['offer_id' => $ids]);
        $request->setPagination((new CatalogCacheRequestBodyPagination())->setLimit(count($ids)));

        if ($this->loadImages) {
            $request->setInclude(['images']);
        }

        return $this->offersApi->searchElasticOffersAsync($request)
            ->then(function (SearchElasticOffersResponse $response) {
                $this->offers = collect($response->getData())->keyBy(function (ElasticOffer $offer) {
                    return $offer->getId();
                });
            });
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "images":
                    $this->loadImages = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
