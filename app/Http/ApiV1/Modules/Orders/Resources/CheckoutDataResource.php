<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Domain\Orders\Data\Orders\CheckoutData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CheckoutData
 */
class CheckoutDataResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'payment_systems' => $this->paymentSystems,
            'delivery_methods' => $this->deliveryMethods,
        ];
    }
}
