<?php

namespace App\Http\ApiV1\Modules\Orders\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\OrderCommitResponseData;

/**
 * @mixin OrderCommitResponseData
 */
class CommitOrderResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'order_id' => $this->getOrderId(),
            'payment_url' => $this->whenNotNull($this->getPaymentLink()),
        ];
    }
}
