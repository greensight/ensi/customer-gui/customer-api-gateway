<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers;

use App\Domain\Orders\Actions\Oms\CheckoutDataAction;
use App\Domain\Orders\Actions\Oms\CommitOrderAction;
use App\Domain\Orders\Actions\Oms\GetQrImageAction;
use App\Http\ApiV1\Modules\Orders\Queries\CustomerOrdersQuery;
use App\Http\ApiV1\Modules\Orders\Queries\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\CommitOrderRequest;
use App\Http\ApiV1\Modules\Orders\Requests\GetCheckoutDataRequest;
use App\Http\ApiV1\Modules\Orders\Resources\CheckoutDataResource;
use App\Http\ApiV1\Modules\Orders\Resources\CommitOrderResource;
use App\Http\ApiV1\Modules\Orders\Resources\CustomerOneOrderResource;
use App\Http\ApiV1\Modules\Orders\Resources\CustomerOrdersResource;
use App\Http\ApiV1\Modules\Orders\Resources\OrdersResource;
use Exception;
use Illuminate\Contracts\Support\Responsable;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrdersController
{
    public function commit(CommitOrderRequest $request, CommitOrderAction $action): Responsable
    {
        return CommitOrderResource::make($action->execute($request->validated()));
    }

    public function get(int $id, OrdersQuery $query): Responsable
    {
        return OrdersResource::make($query->findOrFail($id));
    }

    public function getCheckoutData(GetCheckoutDataRequest $request, CheckoutDataAction $action): Responsable
    {
        return CheckoutDataResource::make($action->execute($request->convertToObject()));
    }

    /**
     * @throws Exception
     */
    public function getQrImage(int $id, GetQrImageAction $action): BinaryFileResponse
    {
        $path = $action->execute($id);

        if (!file_exists($path)) {
            throw new NotFoundHttpException();
        }

        return response()->file($path);
    }

    public function customerOrdersList(CustomerOrdersQuery $query): Responsable
    {
        return CustomerOrdersResource::collectPage($query->get());
    }

    public function customerOrdersGet(int $id, CustomerOrdersQuery $query): Responsable
    {
        return CustomerOneOrderResource::make($query->findOrFail($id));
    }
}
