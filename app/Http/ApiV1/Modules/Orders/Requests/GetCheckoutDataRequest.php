<?php

namespace App\Http\ApiV1\Modules\Orders\Requests;

use App\Domain\Orders\Actions\Oms\Data\CheckoutRequestData;
use App\Http\ApiV1\OpenApiGenerated\Enums\CountryCodeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class GetCheckoutDataRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_method' => ['required', 'integer', Rule::in(DeliveryMethodEnum::getAllowableEnumValues())],

            'delivery_address' => ['required', 'array'],

            "delivery_address.address_string" => ['required_with:delivery_address', 'string'],
            "delivery_address.country_code" => ['required_with:delivery_address', 'string', new Enum(CountryCodeEnum::class)],
            "delivery_address.post_index" => ['required_with:delivery_address', 'string'],
            "delivery_address.region" => ['required_with:delivery_address', 'string'],
            "delivery_address.region_guid" => ['required_with:delivery_address', 'string'],
            "delivery_address.area" => ['nullable', 'string'],
            "delivery_address.area_guid" => ['nullable', 'string'],
            "delivery_address.city" => ['nullable', 'string'],
            "delivery_address.city_guid" => ['nullable', 'string'],
            "delivery_address.street" => ['nullable', 'string'],
            "delivery_address.house" => ['nullable', 'string'],
            "delivery_address.block" => ['nullable', 'string'],
            "delivery_address.flat" => ['nullable', 'string'],
            "delivery_address.floor" => ['nullable', 'string'],
            "delivery_address.porch" => ['nullable', 'string'],
            "delivery_address.intercom" => ['nullable', 'string'],
            "delivery_address.geo_lat" => ['nullable', 'string'],
            "delivery_address.geo_lon" => ['nullable', 'string'],
        ];
    }

    public function convertToObject(): CheckoutRequestData
    {
        return new CheckoutRequestData(
            $this->integer('delivery_method'),
            $this->input('delivery_address'),
        );
    }
}
