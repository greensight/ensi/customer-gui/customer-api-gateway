<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Расположение кнопки:
 * * `top_left` - Сверху слева
 * * `top` - Сверху
 * * `top_right` - Сверху справа
 * * `right` - Справа
 * * `bottom_right` - Снизу справа
 * * `bottom` - Снизу
 * * `bottom_left` - Снизу слева
 * * `left` - Слева
 */
enum CmsBannerButtonLocationEnum: string
{
    /** Сверху слева */
    case LOCATION_TOP_LEFT = 'top_left';
    /** Сверху */
    case LOCATION_TOP = 'top';
    /** Сверху справа */
    case LOCATION_TOP_RIGHT = 'top_right';
    /** Справа */
    case LOCATION_RIGHT = 'right';
    /** Снизу справа */
    case LOCATION_BOTTOM_RIGHT = 'bottom_right';
    /** Снизу */
    case LOCATION_BOTTOM = 'bottom';
    /** Снизу слева */
    case LOCATION_BOTTOM_LEFT = 'bottom_left';
    /** Слева */
    case LOCATION_LEFT = 'left';
}
