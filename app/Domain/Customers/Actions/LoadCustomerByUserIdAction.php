<?php

namespace App\Domain\Customers\Actions;

use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Date;

class LoadCustomerByUserIdAction
{
    public function __construct(private readonly CustomersApi $customersApi)
    {
    }

    public function execute(int $userId): ?Customer
    {
        return Cache::get($this->cacheKey($userId), function () use ($userId) {
            $request = new SearchCustomersRequest();
            $request->setFilter((object) ['user_id' => $userId]);

            $customer = $this->customersApi->searchCustomer($request)->getData();
            Cache::put($this->cacheKey($userId), $customer, Date::now()->addHour());

            return $customer;
        });
    }

    private function cacheKey(int $userId): string
    {
        return 'CUSTOMER_USER_ID_' . $userId;
    }
}
