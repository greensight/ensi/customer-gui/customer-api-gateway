<?php

declare(strict_types=1);

namespace App\Domain\Auth\Exceptions;

use RuntimeException;
use Throwable;

class AuthUserException extends RuntimeException
{
    protected $message;
    protected int $seconds;
    protected ?string $textCode;

    public function __construct(string $message = "", int $seconds = 0, string $textCode = null, int $code = 400, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->seconds = $seconds;
        $this->textCode = $textCode;
    }

    public function getSeconds(): int
    {
        return $this->seconds;
    }

    public function getTextCode(): ?string
    {
        return $this->textCode;
    }
}
