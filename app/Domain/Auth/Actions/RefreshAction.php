<?php

namespace App\Domain\Auth\Actions;

use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\Dto\CreateTokenRequest;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;
use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;

class RefreshAction
{
    public function __construct(private readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $refreshToken): CreateTokenResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::REFRESH_TOKEN);
        $request->setClientId(
            config('openapi-clients.customers.customer-auth.client.id')
        );
        $request->setClientSecret(
            config('openapi-clients.customers.customer-auth.client.secret')
        );
        $request->setRefreshToken($refreshToken);

        return $this->oauthApi->createToken($request);
    }
}
