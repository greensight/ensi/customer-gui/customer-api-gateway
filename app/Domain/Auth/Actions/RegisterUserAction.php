<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Exceptions\AuthUserException;
use App\Exceptions\ResponseValidationException;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\RegisterRequest;
use Ensi\CustomerAuthClient\Dto\RegisterValidationTypeEnum;

class RegisterUserAction
{
    public function __construct(protected readonly UsersApi $usersApi)
    {
    }

    /**
     * @throws ResponseValidationException
     */
    public function execute(array $fields): void
    {
        $request = new RegisterRequest($fields);
        $request->setValidationType(empty($fields['code'])
            ? RegisterValidationTypeEnum::PASSWORD
            : RegisterValidationTypeEnum::CODE_BY_PHONE);

        try {
            $this->usersApi->register($request);
        } catch (ApiException $e) {
            $errorMessages = array_column($e->getResponseErrors(), 'message');
            $errorCodes = array_column($e->getResponseErrors(), 'code');

            throw new AuthUserException(implode('; ', $errorMessages), 0, implode('; ', $errorCodes));
        }
    }
}
