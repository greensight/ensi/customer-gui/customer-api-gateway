<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Actions\Data\LoginRequestData;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\Dto\CreateTokenRequest;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;
use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class LoginAction
{
    public function __construct(private readonly OauthApi $oauthApi)
    {
    }

    public function execute(LoginRequestData $data): CreateTokenResponse
    {
        $request = new CreateTokenRequest();

        $request->setClientId(
            config('openapi-clients.customers.customer-auth.client.id')
        );
        $request->setClientSecret(
            config('openapi-clients.customers.customer-auth.client.secret')
        );

        match ($data->grantType) {
            GrantTypeEnum::PASSWORD => $this->loginByPassword($request, $data),
            GrantTypeEnum::CONFIRMATION_CODE_BY_PHONE => $this->loginByCode($request, $data),
            default => throw new BadRequestException()
        };

        $request->setGrantType($data->grantType);

        return $this->oauthApi->createToken($request);
    }

    private function loginByPassword(CreateTokenRequest $request, LoginRequestData $data): void
    {
        $request->setUsername($data->login);
        $request->setPassword($data->password);
    }

    private function loginByCode(CreateTokenRequest $request, LoginRequestData $data): void
    {
        $request->setPhone($data->phone);
        $request->setCode($data->code);
    }
}
