<?php

namespace App\Domain\Auth\Actions\Data;

use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;

class LoginRequestData
{
    public function __construct(
        public ?string $login = null,
        public ?string $password = null,
        public ?string $phone = null,
        public ?string $code = null,
        public ?string $grantType = null
    ) {
        $this->grantType = !empty($this->password) ? GrantTypeEnum::PASSWORD : GrantTypeEnum::CONFIRMATION_CODE_BY_PHONE;
    }

    public function toArray(): array
    {
        return [
            'login' => $this->login,
            'password' => $this->password,
            'code' => $this->code,
            'phone' => $this->phone,
            'grantType' => $this->grantType,
        ];
    }
}
