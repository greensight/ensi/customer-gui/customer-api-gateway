<?php

namespace App\Domain\Catalog\Tests\Factories\Cloud;

use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\CatalogSearchResponse;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\Data\CatalogSearchData;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;

class CatalogSearchFactory extends BaseApiFactory
{
    protected ?array $products = null;

    protected function definition(): array
    {
        $totalProducts = isset($this->products) ? count($this->products) : $this->faker->numberBetween(0, 20);
        $totalCategories = $this->faker->numberBetween(0, 20);

        return [
            'products' => $this->products ?? $this->faker->randomList(
                f: fn () => $this->faker->ean8(),
                min: $totalProducts,
                max: $totalProducts
            ),
            'total_products' => $totalProducts + $this->faker->numberBetween(0, 1000),
            'categories' => $this->faker->randomList(
                f: fn () => $this->faker->ean8(),
                min: $totalCategories,
                max: $totalCategories
            ),
            'total_categories' => $totalCategories + $this->faker->numberBetween(0, 1000),
            'correction' => $this->faker->boolean ? $this->faker->word() : new FactoryMissingValue(),
            'product_hints' => $this->faker->boolean ? $this->getHints() : new FactoryMissingValue(),
        ];
    }

    public function withProducts(array $products): self
    {
        return $this
            ->immutableSet('products', $products)
            ->immutableSet('total_product', count($products) + $this->faker->numberBetween(0, 1000));
    }

    public function getHints(?int $count = null): array
    {
        $totalHints = $count ?? $this->faker->numberBetween(0, 10);

        return $this->faker->randomList(
            f: fn () => ['word' => $this->faker->word(), 'hint' => $this->faker->sentence()],
            min: $totalHints,
            max: $totalHints
        );
    }

    public function make(array $extra = []): CatalogSearchData
    {
        return new CatalogSearchData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CatalogSearchResponse
    {
        return new CatalogSearchResponse(['data' => $this->makeArray($extra)]);
    }
}
