<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\ElasticDiscount;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class ElasticDiscountFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): ElasticDiscount
    {
        return new ElasticDiscount($this->makeArray($extra));
    }
}
