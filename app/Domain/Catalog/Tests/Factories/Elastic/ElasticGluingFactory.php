<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\Gluing;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ElasticGluingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence,
            'code' => $this->faker->slug,
            'props' => $this->faker->randomList(fn () => ElasticGluingPropertyFactory::new()->make(), min: 1, max: 2),
        ];
    }

    public function make(array $extra = []): Gluing
    {
        return new Gluing($this->makeArray($extra));
    }
}
