<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\GluingProperty;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class ElasticGluingPropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'prop_type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'prop_name' => $this->faker->sentence,
            'prop_code' => $this->faker->slug,
            'value_value' => $this->faker->word,
            'value_name' => $this->faker->nullable()->sentence,
        ];
    }

    public function make(array $extra = []): GluingProperty
    {
        return new GluingProperty($this->makeArray($extra));
    }
}
