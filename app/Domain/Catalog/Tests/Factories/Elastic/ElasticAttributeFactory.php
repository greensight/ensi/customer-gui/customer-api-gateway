<?php

namespace App\Domain\Catalog\Tests\Factories\Elastic;

use Ensi\CatalogCacheClient\Dto\ElasticAttribute;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class ElasticAttributeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues());

        return [
            'type' => $type,
            'name' => $this->faker->sentence,
            'code' => $this->faker->slug,
            'values' => $this->faker->randomList(fn () => ElasticAttributeValueFactory::new()->valueByType($type)->make(), min: 1, max: 2),
        ];
    }

    public function make(array $extra = []): ElasticAttribute
    {
        return new ElasticAttribute($this->makeArray($extra));
    }
}
