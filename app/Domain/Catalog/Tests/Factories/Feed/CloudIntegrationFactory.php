<?php

namespace App\Domain\Catalog\Tests\Factories\Feed;

use Ensi\FeedClient\Dto\CloudIntegration;
use Ensi\FeedClient\Dto\CloudIntegrationResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CloudIntegrationFactory extends BaseApiFactory
{
    private ?bool $integration = null;

    protected function definition(): array
    {
        $integration = $this->integration ?? $this->faker->boolean;

        return [
            'integration' => $integration,
            'private_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),
            'public_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function active(bool $integration = true): self
    {
        $this->integration = $integration;

        return $this;
    }

    public function make(array $extra = []): CloudIntegration
    {
        return new CloudIntegration($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CloudIntegrationResponse
    {
        return new CloudIntegrationResponse(['data' => $this->make($extra)]);
    }
}
