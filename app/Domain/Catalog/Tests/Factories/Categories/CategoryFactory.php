<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\CategoryResponse;
use Ensi\PimClient\Dto\SearchCategoriesResponse;

class CategoryFactory extends BaseApiFactory
{
    public ?int $propertiesCount = 0;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
            'is_real_active' => $this->faker->boolean,
            'name' => $this->faker->sentence(2),
            'code' => $this->faker->slug(2),
            'is_inherits_properties' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
            'parent_id' => $this->faker->modelId(),
            'properties' => $this->when(
                $this->propertiesCount > 0,
                fn () => BoundPropertyFactory::new()->makeSeveral($this->propertiesCount)
            ),
        ];
    }

    public function make(array $extra = []): Category
    {
        return new Category($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CategoryResponse
    {
        return new CategoryResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCategoriesResponse
    {
        return $this->generateResponseSearch(SearchCategoriesResponse::class, $extras, $count, $pagination);
    }

    public function withProperties(int $count): self
    {
        return $this->immutableSet('propertiesCount', $count);
    }
}
