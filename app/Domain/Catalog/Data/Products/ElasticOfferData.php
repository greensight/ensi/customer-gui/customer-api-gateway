<?php

namespace App\Domain\Catalog\Data\Products;

use App\Domain\Crm\Contracts\FavoriteEntity;
use App\Domain\Crm\Traits\Favorite;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\PimClient\Dto\ProductTypeEnum;

class ElasticOfferData implements FavoriteEntity
{
    use Favorite;

    public function __construct(public readonly ElasticOffer $offer)
    {
    }

    public function getLinkFavoriteId(): int
    {
        return $this->offer->getProductId();
    }

    public function isWeight(): bool
    {
        return $this->offer->getType() === ProductTypeEnum::WEIGHT;
    }
}
