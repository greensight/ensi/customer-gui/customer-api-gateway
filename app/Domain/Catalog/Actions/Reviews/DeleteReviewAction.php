<?php

namespace App\Domain\Catalog\Actions\Reviews;

use App\Domain\Catalog\Exceptions\AccessException;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\Dto\Review;

class DeleteReviewAction
{
    public function __construct(protected readonly ReviewsApi $reviewsApi)
    {
    }

    public function execute(int $id): void
    {
        $review = $this->reviewsApi->getReview($id)->getData();

        if (!$this->isAccessToReviewByUser($review)) {
            throw new AccessException('Forbidden');
        }

        $this->reviewsApi->deleteReview($id);
    }

    private function isAccessToReviewByUser(Review $review): bool
    {
        return user()->customerId === $review->getCustomerId();
    }
}
