<?php

namespace App\Domain\Catalog\Actions\Offers;

use App\Domain\Catalog\Actions\Cloud\CheckCloudIntegrationAction;
use App\Domain\Catalog\Actions\Cloud\SearchProducts\Data\SearchProductsData;
use App\Domain\Catalog\Actions\Cloud\SearchProducts\SearchProductsAction;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ElasticOffersQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\SearchOffersRequest;
use App\Http\ApiV1\Support\Pagination\Page;

class SearchOffersAction
{
    public function __construct(
        protected readonly CheckCloudIntegrationAction $checkCloudApi,
        protected readonly SearchProductsAction $searchByCloudAction,
        protected readonly ElasticOffersQuery $query,
    ) {
    }

    public function execute(SearchOffersRequest $httpRequest): Page
    {
        $searchByCloudApi = $this->checkCloudApi->execute();

        return $searchByCloudApi
            ? $this->searchByCloudAction->execute(new SearchProductsData($httpRequest))
            : $this->query->get();
    }
}
