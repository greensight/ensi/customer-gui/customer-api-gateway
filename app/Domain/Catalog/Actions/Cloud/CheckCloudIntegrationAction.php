<?php

namespace App\Domain\Catalog\Actions\Cloud;

use Ensi\CloudApiSdk\Configuration;
use Ensi\FeedClient\Api\CloudIntegrationsApi;
use Ensi\FeedClient\ApiException;
use Ensi\FeedClient\Dto\CloudIntegration;
use Exception;

class CheckCloudIntegrationAction
{
    private const CLOUD_INTEGRATION_NOT_FOUND_CODE = 404;

    public function __construct(
        protected readonly CloudIntegrationsApi $cloudIntegrationsApi,
        protected readonly Configuration $configuration,
    ) {
    }

    public function execute(): bool
    {
        if ($this->configuration->getPublicToken() && $this->configuration->getPrivateToken()) {
            return true;
        }

        $integration = $this->getCloudIntegrationData();
        if (!$integration?->getIntegration()) {
            return false;
        }

        $this->configuration
            ->setPublicToken($integration->getPublicApiKey())
            ->setPrivateToken($integration->getPrivateApiKey());

        return true;
    }

    protected function getCloudIntegrationData(): ?CloudIntegration
    {
        try {
            return $this->cloudIntegrationsApi
                ->getCloudIntegration()
                ->getData();

        } catch (ApiException $e) {
            if ($e->getCode() !== self::CLOUD_INTEGRATION_NOT_FOUND_CODE) {
                logger()->error("Ошибка при получении данных из Cloud Api", [
                    "code" => $e->getCode(),
                    "message" => $e->getMessage(),
                ]);
            }
        } catch (Exception $e) {
            logger()->error("Ошибка при получении данных из Cloud Api", [
                "message" => $e->getMessage(),
            ]);
        }

        return null;
    }
}
