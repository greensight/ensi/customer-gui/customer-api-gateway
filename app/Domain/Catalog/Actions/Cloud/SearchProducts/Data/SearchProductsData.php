<?php

namespace App\Domain\Catalog\Actions\Cloud\SearchProducts\Data;

use App\Domain\Catalog\Data\Products\ElasticOfferData;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\SearchOffersRequest;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Response\CatalogSearchResponse;
use Illuminate\Support\Collection;

class SearchProductsData
{
    public CatalogSearchResponse $cloudResponse;
    /** @var Collection<ElasticOfferData> */
    public Collection $offers;

    public function __construct(protected SearchOffersRequest $httpRequest)
    {
        $this->offers = collect();
    }

    public function getFilter(): array
    {
        return $this->httpRequest->get('filter', []);
    }

    public function getInclude(): array
    {
        $include = $this->httpRequest->get('include', []);

        if (is_string($include)) {
            $include = explode(',', $include);
        }

        return $include;
    }

    public function getPaginationLimit(): int
    {
        $httpPagination = $this->httpRequest->get('pagination', []);

        return $httpPagination['limit'] ?? 10;
    }

    public function getPaginationOffset(): int
    {
        $httpPagination = $this->httpRequest->get('pagination', []);

        return $httpPagination['offset'] ?? 0;
    }
}
