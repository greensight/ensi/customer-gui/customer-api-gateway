<?php

namespace App\Domain\Catalog\Actions\Cloud\SearchProducts\Steps;

use App\Domain\Catalog\Actions\Cloud\SearchProducts\Data\SearchProductsData;
use Ensi\CloudApiSdk\Api\CatalogApi;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\CatalogSearchRequest;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Filter\CatalogSearchFilter;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Filter\CatalogSearchPropertyFilter;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Include\CatalogSearchIncludeEnum;
use Ensi\CloudApiSdk\Dto\Catalog\CatalogSearch\Request\Pagination\CatalogSearchPagination;

class SearchCloudProductsAction
{
    private const GLUING_IS_MAIN_PROP_NAME = 'Главный товар';
    private const GLUING_IS_MAIN_PROP_YES = 'да';
    private const GLUING_IS_MAIN_PROP_NO = 'нет';

    protected CatalogSearchRequest $request;

    public function __construct(
        protected readonly LoadBrandNamesAction $getBrandNamesAction,
        protected readonly CatalogApi $cloudApi,
    ) {
    }

    public function execute(SearchProductsData $data): void
    {
        $this->request = new CatalogSearchRequest();

        $this->fillPagination($data->getPaginationLimit(), $data->getPaginationOffset());
        $this->fillFilters($data->getFilter());
        $this->fillInclude();
        $this->fillCustomerId();

        $data->cloudResponse = $this->cloudApi->search($this->request);
    }

    private function fillFilters(array $httpFilters): void
    {
        $filter = new CatalogSearchFilter();
        $filter->location_id = '1'; // Хардкод по ФЗ
        $filter->query = $httpFilters['name'];

        if (!empty($httpFilters['category_id'])) {
            $categoryIds = (array)$httpFilters['category_id'];
            $filter->category_ids = array_map('strval', $categoryIds);
        }

        if (!empty($httpFilters['brand_id'])) {
            $brandIds = (array)$httpFilters['brand_id'];
            $filter->brands = $this->getBrandNamesAction->execute($brandIds);
        }

        if (isset($httpFilters['gluing_is_main'])) {
            $prop = new CatalogSearchPropertyFilter();
            $prop->name = self::GLUING_IS_MAIN_PROP_NAME;
            $prop->values = [
                $httpFilters['gluing_is_main']
                    ? self::GLUING_IS_MAIN_PROP_YES
                    : self::GLUING_IS_MAIN_PROP_NO,
            ];

            $filter->properties = [$prop];
        }

        $this->request->filter = $filter;
    }

    private function fillInclude(): void
    {
        $this->request->include = [
            CatalogSearchIncludeEnum::PRODUCTS,
            CatalogSearchIncludeEnum::CORRECTION,
        ];
    }

    private function fillPagination(int $limit, int $offset): void
    {
        $pagination = new CatalogSearchPagination();
        $pagination->limit_products = $limit;
        $pagination->offset_products = $offset;

        $this->request->pagination = $pagination;
    }

    private function fillCustomerId(): void
    {
        $customerId = user()?->customerId;
        if ($customerId) {
            $this->request->customerId = (string)$customerId;
        }
    }
}
