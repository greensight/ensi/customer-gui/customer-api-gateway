<?php

namespace App\Domain\Catalog\Actions\Cloud\SearchProducts\Steps;

use App\Domain\Catalog\Actions\Cloud\SearchProducts\Data\SearchProductsData;
use App\Domain\Catalog\Data\Products\ElasticOfferData;
use Ensi\CatalogCacheClient\Api\ElasticOffersApi;
use Ensi\CatalogCacheClient\Dto\ElasticOffer;
use Ensi\CatalogCacheClient\Dto\SearchElasticOffersRequest;

class SearchCatalogCacheOffersAction
{
    protected SearchElasticOffersRequest $request;

    public function __construct(
        protected readonly ElasticOffersApi $offersApi,
    ) {
    }

    public function execute(SearchProductsData $data): void
    {
        $products = $data->cloudResponse->data->products;
        if (empty($products)) {
            return;
        }

        $this->request = new SearchElasticOffersRequest();
        $this->fillFilter($products);
        $this->fillInclude($data);
        $response = $this->offersApi->searchElasticOffers($this->request);

        $data->offers = collect($response->getData())->map(fn (ElasticOffer $offer) => new ElasticOfferData($offer));
    }

    protected function fillFilter(array $productIds): void
    {
        $this->request->setFilter((object)[
            'product_id' => array_map('intval', $productIds),
        ]);
    }

    protected function fillInclude(SearchProductsData $data): void
    {
        $httpInclude = $data->getInclude();
        if ($httpInclude) {
            $this->request->setInclude($httpInclude);
        }
    }
}
