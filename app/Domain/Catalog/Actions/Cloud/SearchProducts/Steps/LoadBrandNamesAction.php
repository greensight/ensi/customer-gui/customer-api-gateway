<?php

namespace App\Domain\Catalog\Actions\Cloud\SearchProducts\Steps;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchBrandsRequest;

class LoadBrandNamesAction
{
    public function __construct(
        protected readonly BrandsApi $brandsApi
    ) {
    }

    public function execute(array $brandIds): array
    {
        if (empty($brandIds)) {
            return [];
        }

        $request = (new SearchBrandsRequest())
            ->setFilter((object)['id' => $brandIds])
            ->setPagination(
                (new RequestBodyPagination())->setLimit(count($brandIds))
                    ->setType(PaginationTypeEnum::CURSOR)
            );

        $response = $this->brandsApi->searchBrands($request);

        return array_map(fn (Brand $brand) => $brand->getName(), $response->getData());
    }
}
