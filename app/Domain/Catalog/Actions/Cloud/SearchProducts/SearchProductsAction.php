<?php

namespace App\Domain\Catalog\Actions\Cloud\SearchProducts;

use App\Domain\Catalog\Actions\Cloud\SearchProducts\Data\SearchProductsData;
use App\Domain\Catalog\Actions\Cloud\SearchProducts\Steps\SearchCatalogCacheOffersAction;
use App\Domain\Catalog\Actions\Cloud\SearchProducts\Steps\SearchCloudProductsAction;
use App\Domain\Crm\Actions\Favorites\EnrichFavoriteEntityAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\Page;

class SearchProductsAction
{
    public function __construct(
        protected readonly SearchCloudProductsAction $searchCloudProductsAction,
        protected readonly SearchCatalogCacheOffersAction $searchCatalogCacheOffersAction,
        protected readonly EnrichFavoriteEntityAction $enrichFavoriteAction,
    ) {
    }

    public function execute(SearchProductsData $data): Page
    {
        $this->searchCloudProductsAction->execute($data);
        $this->searchCatalogCacheOffersAction->execute($data);
        $this->enrichFavoriteAction->execute($data->offers);

        return new Page($data->offers, [
            "limit" => $data->getPaginationLimit(),
            "offset" => $data->getPaginationOffset(),
            "total" => $data->cloudResponse->data->total_products,
            "type" => PaginationTypeEnum::OFFSET->value,
        ]);
    }
}
