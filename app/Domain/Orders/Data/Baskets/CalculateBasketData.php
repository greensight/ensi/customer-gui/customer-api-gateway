<?php

namespace App\Domain\Orders\Data\Baskets;

use Ensi\BasketsClient\Dto\CalculateBasket;
use Illuminate\Support\Collection;

class CalculateBasketData
{
    /** @var Collection<CalculateBasketItemData> */
    protected Collection $items;

    public function __construct(public readonly CalculateBasket $basket)
    {
        $this->items = collect();
    }

    public function addItem(CalculateBasketItemData $item): void
    {
        $this->items->add($item);
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public static function make(CalculateBasket $basket): self
    {
        $basketData = new self($basket);
        foreach ($basket->getItems() as $item) {
            $itemData = new CalculateBasketItemData($item);
            $basketData->addItem($itemData);
        }

        return $basketData;
    }
}
