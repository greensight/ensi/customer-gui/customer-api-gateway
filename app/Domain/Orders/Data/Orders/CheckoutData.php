<?php

namespace App\Domain\Orders\Data\Orders;

class CheckoutData
{
    public function __construct(
        public readonly array $paymentSystems,
        public readonly array $deliveryMethods
    ) {
    }
}
