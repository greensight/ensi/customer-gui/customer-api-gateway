<?php

namespace App\Domain\Orders\Tests\Oms\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderCommitResponse;
use Ensi\OmsClient\Dto\OrderCommitResponseData;

class OrderCommitFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_id' => $this->faker->modelId(),
            'payment_url' => $this->faker->nullable()->url(),
        ];
    }

    public function make(array $extra = []): OrderCommitResponseData
    {
        return new OrderCommitResponseData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OrderCommitResponse
    {
        return new OrderCommitResponse(['data' => $this->make($extra)]);
    }
}
