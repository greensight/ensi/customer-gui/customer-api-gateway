<?php

namespace App\Domain\Orders\Tests\Baskets\Factories;

use Ensi\BasketsClient\Dto\CalculateBasket;
use Ensi\BasketsClient\Dto\CalculateBasketItem;
use Ensi\BasketsClient\Dto\SearchBasketCalculateResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\PromoCodeApplyStatusEnum;

class BasketCalculateFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        return [
            'items' => $this->items,
            'promo_code' => $this->faker->nullable()->word(),
            'promo_code_apply_status' => $this->faker->nullable()->randomElement(PromoCodeApplyStatusEnum::getAllowableEnumValues()),
        ];
    }

    public function withPromoCodeStatus(string $status = PromoCodeApplyStatusEnum::SUCCESS): static
    {
        return $this->state(['promo_code_apply_status' => $status]);
    }

    public function make(array $extra = []): CalculateBasket
    {
        return new CalculateBasket($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): SearchBasketCalculateResponse
    {
        return new SearchBasketCalculateResponse(['data' => $this->make($extra)]);
    }

    public function withItems(?CalculateBasketItem $item = null): self
    {
        $this->items[] = $item ?: CalculateBasketItemFactory::new()->make();

        return $this;
    }
}
