<?php

namespace App\Domain\Orders\Tests\Baskets\Factories;

use Ensi\BasketsClient\Dto\CalculateBasketItem;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;

class CalculateBasketItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $pricePerOne = $this->faker->numberBetween(1, 10000);
        $costPerOne = $this->faker->numberBetween($pricePerOne, $pricePerOne + 1000);

        $qty = $this->faker->randomFloat(2, 1, 100);

        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'barcode' => $this->faker->ean13(),
            'vendor_code' => $this->faker->numerify('######'),
            'type' => $type,
            'is_adult' => $this->faker->boolean(),
            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomElement(ProductTariffingVolumeEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'main_image' => $this->faker->nullable()->url,
            'qty' => $qty,
            'price_per_one' => $pricePerOne,
            'price' => round($pricePerOne * $qty),
            'cost_per_one' => $costPerOne,
            'cost' => round($costPerOne * $qty),
            'discounts' => $this->faker->randomList(fn () => CalculatedDiscountFactory::new()->make()),
        ];
    }

    public function make(array $extra = []): CalculateBasketItem
    {
        return new CalculateBasketItem($this->makeArray($extra));
    }
}
