<?php

namespace App\Domain\Orders\Actions\Oms;

use App\Domain\Orders\Actions\Oms\Data\CheckoutRequestData;
use App\Domain\Orders\Data\Orders\CheckoutData;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Dto\CheckoutDeliveryPriceRequest;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\OmsClient\Dto\PaymentSystemEnum;

class CheckoutDataAction
{
    public function __construct(private readonly DeliveryPricesApi $deliveryPricesApi)
    {
    }

    public function execute(CheckoutRequestData $data): CheckoutData
    {
        $deliveryPrices = $this->getDeliveryPrices($data);
        $deliveryMethodDescriptions = DeliveryMethodEnum::getDescriptions();

        $deliveryMethods = [];
        /** @var DeliveryPrice $deliveryPrice */
        foreach ($deliveryPrices as $deliveryPrice) {
            $deliveryMethods[] = [
                'id' => $deliveryPrice->getDeliveryMethod(),
                'name' => $deliveryMethodDescriptions[$deliveryPrice->getDeliveryMethod()],
                'price' => $deliveryPrice->getPrice(),
            ];
        }

        return new CheckoutData(
            PaymentSystemEnum::getAllowableEnumValues(),
            $deliveryMethods
        );
    }

    private function getDeliveryPrices(CheckoutRequestData $data): array
    {
        $request = new CheckoutDeliveryPriceRequest();
        $request->setDeliveryMethod($data->deliveryMethod);
        $request->setRegionGuid($data->deliveryAddress['region_guid']);

        return $this->deliveryPricesApi->checkoutDeliveryPrices($request)->getData();
    }
}
