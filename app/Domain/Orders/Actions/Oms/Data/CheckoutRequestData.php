<?php

namespace App\Domain\Orders\Actions\Oms\Data;

class CheckoutRequestData
{
    public function __construct(
        public int $deliveryMethod,
        public array $deliveryAddress,
    ) {
    }
}
