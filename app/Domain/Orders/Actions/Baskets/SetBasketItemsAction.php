<?php

namespace App\Domain\Orders\Actions\Baskets;

use Ensi\BasketsClient\Api\CustomerBasketsApi;
use Ensi\BasketsClient\Dto\SetBasketItemRequest;

class SetBasketItemsAction
{
    public function __construct(protected readonly CustomerBasketsApi $basketApi)
    {
    }

    public function execute(array $items): void
    {
        $request = new SetBasketItemRequest();
        $request->setCustomerId(user()->customerId);
        $request->setItems($items);

        $this->basketApi->setBasketCustomerItem($request);
    }
}
