<?php

namespace App\Domain\Orders\Actions\Baskets;

use Ensi\BasketsClient\Api\CustomerBasketsApi;
use Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest;

class DeleteBasketAction
{
    public function __construct(protected readonly CustomerBasketsApi $basketApi)
    {
    }

    public function execute(): void
    {
        $request = new DeleteBasketCustomerRequest();
        $request->setCustomerId(user()->customerId);

        $this->basketApi->deleteBasketCustomer($request);
    }
}
