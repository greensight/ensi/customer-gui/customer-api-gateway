<?php

namespace App\Domain\Common\Exceptions;

use RuntimeException;

class AccessException extends RuntimeException
{
}
