<?php

namespace App\Domain\Common\Tests\Factories;

use Ensi\LaravelTestFactories\Factory;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class JWTFactory extends Factory
{
    protected function definition(): array
    {
        $configuration = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::base64Encoded($this->faker->sha256()),
        );

        return [
            'jwt' => $configuration->builder()
                ->identifiedBy($this->faker->md5())
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString(),
        ];
    }

    public function make(array $extra = []): string
    {
        return $this->makeArray($extra)['jwt'];
    }
}
