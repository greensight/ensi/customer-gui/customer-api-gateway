<?php

namespace App\Domain\Crm\Actions\Favorites;

use App\Exceptions\ValidateException;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\PaginationTypeEnum;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\RequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Illuminate\Support\Collection;

class DeleteCustomerFavoritesAction
{
    public function __construct(
        protected readonly CustomerFavoritesApi $favoritesApi,
        protected readonly OffersApi $offersApi,
    ) {
    }

    public function execute(array $offerIds): void
    {
        $offers = $this->loadOffers($offerIds);

        if ($offers->count() < count($offerIds)) {
            throw new ValidateException('В запросе присутствует несуществующий оффер');
        }

        $productIds = $offers->pluck('product_id')->unique()->all();

        $request = new DeleteCustomerFavoritesRequest();
        $request->setCustomerId(user()->customerId);
        $request->setProductIds($productIds);

        $this->favoritesApi->deleteCustomerFavorites($request);
    }

    protected function loadOffers(array $ids): Collection
    {
        $request = new SearchOffersRequest();
        $request->setFilter((object)['id' => $ids]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($ids))->setType(PaginationTypeEnum::CURSOR));

        return collect($this->offersApi->searchOffers($request)->getData());
    }
}
