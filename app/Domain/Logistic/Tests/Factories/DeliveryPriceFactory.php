<?php

namespace App\Domain\Logistic\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse;

class DeliveryPriceFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        return [
            'federal_district_id' => $this->faker->modelId(),
            'region_id' => $this->faker->modelId(),
            'region_guid' => $this->faker->uuid(),
            'price' => $this->faker->numberBetween(500, 1000),
            'delivery_service' => $this->faker->optional()->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_method' => $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): DeliveryPrice
    {
        return new DeliveryPrice($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryPriceResponse
    {
        return new DeliveryPriceResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveryPricesResponse
    {
        return $this->generateResponseSearch(SearchDeliveryPricesResponse::class, $extras, $count, $pagination);
    }
}
