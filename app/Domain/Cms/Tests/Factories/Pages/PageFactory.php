<?php

namespace App\Domain\Cms\Tests\Factories\Pages;

use Ensi\CmsClient\Dto\Page;
use Ensi\CmsClient\Dto\SearchPagesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->words(3, true),
            'url' => $this->faker->slug(),
            'content' => $this->faker->randomHtml(),
        ];
    }

    public function make(array $extra = []): Page
    {
        return new Page($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPagesResponse
    {
        return $this->generateResponseSearch(SearchPagesResponse::class, $extras, $count, $pagination);
    }
}
