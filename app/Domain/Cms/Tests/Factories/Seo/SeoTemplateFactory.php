<?php

namespace App\Domain\Cms\Tests\Factories\Seo;

use Ensi\CmsClient\Dto\SearchSeoTemplatesResponse;
use Ensi\CmsClient\Dto\SeoTemplate;
use Ensi\CmsClient\Dto\SeoTemplateResponse;
use Ensi\CmsClient\Dto\SeoTemplateTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SeoTemplateFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->word(),
            'type' => $this->faker->randomElement(SeoTemplateTypeEnum::getAllowableEnumValues()),
            'header' => $this->faker->text(),
            'title' => $this->faker->nullable()->text(),
            'description' => $this->faker->nullable()->text(),
            'seo_text' => $this->faker->nullable()->text(),
            'is_active' => $this->faker->boolean(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): SeoTemplate
    {
        return new SeoTemplate($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): SeoTemplateResponse
    {
        return new SeoTemplateResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchSeoTemplatesResponse
    {
        return $this->generateResponseSearch(SearchSeoTemplatesResponse::class, $extras, $count, $pagination);
    }
}
