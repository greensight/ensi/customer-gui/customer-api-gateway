<?php

namespace App\Domain\Units\Tests\Bu\Factories;

use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\Store;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            // todo other fields
        ];
    }

    public function make(array $extra = []): Store
    {
        return new Store($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoresResponse
    {
        return $this->generateResponseSearch(SearchStoresResponse::class, $extras, $count, $pagination);
    }
}
