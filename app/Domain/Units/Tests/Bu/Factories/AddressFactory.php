<?php

namespace App\Domain\Units\Tests\Bu\Factories;

use App\Domain\Common\Tests\Factories\BaseAddressFactory;
use Ensi\BuClient\Dto\Address;

class AddressFactory extends BaseAddressFactory
{
    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
