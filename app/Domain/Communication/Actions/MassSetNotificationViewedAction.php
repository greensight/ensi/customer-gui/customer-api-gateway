<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\MassPatchNotificationsRequest;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationFillableProperties;

class MassSetNotificationViewedAction
{
    public function __construct(protected readonly NotificationsApi $notificationsApi)
    {
    }

    public function execute(array $notificationIds): void
    {
        $request = new MassPatchNotificationsRequest();
        $request->setFields($this->makeViewedProperties());
        $request->setFilter((object)$this->makeFilter($notificationIds));

        $this->notificationsApi->massPatchNotifications($request);
    }

    protected function makeViewedProperties(): NotificationFillableProperties
    {
        $properties = new NotificationFillableProperties();
        $properties->setIsViewed(true);

        return $properties;
    }

    private function makeFilter(array $notificationIds): array
    {
        $filter = [];
        $filter['customer_id'] = user()->customerId;
        $filter['channels_like'] = NotificationChannelEnum::STOREFRONT;
        $filter['id'] = $notificationIds;

        return $filter;
    }
}
